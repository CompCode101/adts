#include "StackLL.h"
#include <iostream>
using namespace std;

class Stack::Node
{
	public:
		int data = 0;
		Node* link = nullptr;
};

int Stack::size()
{
	return num_elements;
}

void Stack::push(int val)
{
	Node* newPtr = new Node{val};
	newPtr->link=frontPtr;
	frontPtr=newPtr;
	num_elements++;
	
}

void Stack::pop()
{
	Node* delPtr;
	delPtr=frontPtr;
	frontPtr=frontPtr->link;
	
	delete delPtr;
	num_elements--;
}

int Stack::top()
{
	if (num_elements==0)
	{
		cout<<"Stack is empty";
	}
	else
	{
	return frontPtr->data;
	}
}

Stack::~Stack()
{
	while(num_elements>0)
	{
		pop();
	}
}
