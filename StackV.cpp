#include "StackV.h"
#include <iostream>
using namespace std;

	int Stack::size()
	{
		return data.size();
	}
	
	void Stack::push(int num)
	{
		data.push_back(num);
	}

	void Stack::pop()
	{
		data.pop_back();
	}
	
	int Stack::top()
	{
		if (data.size()>0)
		{
			return data.back();
		}
		else
		{
			cout << "The stack does not have any items" << endl;
		}
	}
	
	void Stack::clear()
	{
		while (data.size()>0)
		{
			data.pop_back();
		}
	}
	
	
	
