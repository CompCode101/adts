#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2


 cout << "Welcome to my List ADT client"<<endl<<endl;

 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
 
	L1.insert(24,1);
	L1.insert(98,2);
	L1.insert(7,3);

	L2.insert(7,1);
	L2.insert(98,2);
	L2.insert(24,3);
	
	cout << "List 1 node 1: " << L1.get(1) << endl;
	cout << "List 1 node 2: " << L1.get(2) << endl;
	cout << "List 1 node 3: " << L1.get(3) << endl;
	cout << "List 1 size: " << L1.size() << endl;
	
	cout << "List 2 node 1: " << L2.get(1) << endl;
	cout << "List 2 node 2: " << L2.get(2) << endl;
	cout << "List 2 node 3: " << L2.get(3) << endl;
	cout << "List 2 size: " << L2.size() << endl;
}
